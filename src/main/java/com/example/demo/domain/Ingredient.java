package com.example.demo.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "ingredients")
public class Ingredient {
    @Id
    @Column(name = "ingredients_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long Id;

    @Column(name = "ingredient_name")
    private String ingredientName;

    @Column(name = "ingredient_measure")
    private String ingredientMeasure;

    public Ingredient() { }

    public Ingredient(Long Id, String ingredientName, String ingredientMeasure) {
        this.Id = Id;
        this.ingredientName = ingredientName;
        this.ingredientMeasure = ingredientMeasure;
    }

    public Long getId() { return Id; }
    public void setId(Long Id) { this.Id = Id; }

    public String getIngredientName() { return ingredientName; }
    public void setIngredientName(String ingredientName) { this.ingredientName = ingredientName; }

    public String getIngredientMeasure() { return ingredientMeasure; }
    public void setIngredientMeasure(String ingredientMeasure) { this.ingredientMeasure = ingredientMeasure; }
}
